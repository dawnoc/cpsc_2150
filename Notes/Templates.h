/*
 * Templates.h
 *
 *  Created on: Sep 18, 2018
 *      Author: Don
 */

#ifndef TEMPLATES_H_
#define TEMPLATES_H_

// C++ is a typed language. To write "general" funtions use the data type as a "parameter" using templates. P. Smith Applied DS in C++

template <class T>
class Counter {
public:
	Counter(T n = T());
	void increment(T d);
	getValue() const;
private:
	T data;
// Constructor
	template<typename T>
	Counter<T>::Counter(T n) {
		data = n;
	}
}
#endif /* TEMPLATES_H_ */

template <class T>
void Counter<T>increment(T d) {
	data = data + d;
}

template<class T>
TCounter<T>::getValue() const {
	return data;
}
