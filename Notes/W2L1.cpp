/**
 * A data structure is a construct in a programming language that stores a collection of data
 * A data structure also has operation on that data
 *
 * ABSTRACT DATA TYPE(ADT)
 *
 * Functional abstraction
 * Information hiding
 * Typical operations on data
 */
struct Node {
	int info;
	Node * next;
};

Node * cons(int x, Node * p) {
	Node *q = new Node;
	q->info = x;
	q->next = p;
	return q;
//	return new Node {x,p};
}

Node * insert(int x, Node * p) {
	if (p == nullptr || x <= p->info) {
		return cons(x, p);
	}
	// x > p->info, so x goes after the 1st node
	p->next = insert(x, p->next);
	return p;
}

/**
 * const Node *cPtr
 * The const keyword means that the pointer cPtr cannot be used to change the contents of the Node
 * You can change the pointer itself ,but not the info or where it is pointing to.
 *
 * If you want a pointer that can be set once and not have its calue changed put const AFTER the *
 * Node first;
 * first.info = 4;
 * first.next = nullptr;
 * Node * const rPtr = &first;
 * illegal
 * rPtr = p;
 * OK
 * rPtr->info = 10;
 *
 * Dummy Head Nodes are used to insert and delete without making the first node a special case
 */
