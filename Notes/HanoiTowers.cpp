/**
 * M(n) = M(n-1) + 1 + M(n-1) = 2M(n-1) + 1
 * M(n) = (2^n) - 1 for all n>= 1
 * Proof (by induction on n)
 * n = 1
 * Is (2^1) - 1 = M(1) = 1	yes
 * Is (2^2) - 1 = M(2) = 3	yes
 * case n > 1	Assume true n = k	ie M(k) = (2^k) - 1
 * 				If we can show it's true for n = k + 1, we'tr done ie M(k+1) = (2^(k + 1)) = (2^(k+1)) - 1
 * 	M(k + 1)	= 2M(k) + 1
 * 				= 2((2^k) - 1) + 1
 * 				= 2(2^k) - 2 + 1
 * 				= (2^(k + 1)) - 1
 */

#include <iostream>
#include <cstdlib>
#include <assert.h>

using namespace std;
int moves = 0;

//move n disks from source to dest using a spare
void move(int n, char source, char dest, char spare) {
	if (n == 1) {
		moves++;
		cout << "Move from " << source << " to " << dest << endl;
		return;
	}
	move(n - 1, source, spare, dest);
	move(1, source, dest, spare);
	move(n - 1, spare, dest, source);
}
int main() {
	int n;
	cout << "Enter number of disks on tower >";
	cin >> n;
	assert(n > 0);
	cout << "\nsource is A destination is C\n";
	move(n, 'A', 'C', 'B');
	cout << "There are " << moves << " moves";
	return 0;
}
