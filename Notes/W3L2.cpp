/**
 * An abstract data type ADT is a theoretical set of specification of a data set and the set of operations that can be performed on the data within a set A.
 * A data type is a formed abstract when it is independent of various concrete implementations.
 *
 * LIFO last in first out
 *
 * 	operations
 * 		add an item to the top push
 * 		remove an item from a top pop
 * 		peek at the top item (return value of topmost element without removing it)
 * 		determine if stack is empty
 * 		destroy the stack
 */

#ifndef STACKA_H
#define STACKA_H
class Stack {
public:
	// create an empty stack
	Stack();
	// add x to the top of the stack
	// exception happens if stack is full
//	 void push(const ItemType& x)throw(StackException);
	void push(int x) throw (StackException);
	bool isEmpty() const;
	// get x from the top of the stack
	// an exception happens if the stack is empty
	int pop() throw (StackException);
	// get top element without changing the stack...exception
	int peek() const throw (StackException);
private:
	static const int MAX_ITEMS = 100;
	int items[MAX_ITEMS];
	int n; // In C+11 we can initialize here
};

#include "StackA.h"
Stack::Stack() :
		n(0) {

}
bool Stack::IsEmpty() const {
	return n == 0;
}
void Stack::push(int x) throw (StackException) {
	if (n == MAX_ITEMS) {
		throw StackException("StackException:stack overflow");
	} else {
		items[n] = x;
		n++;
	}
}


int Stack::pop throw (StackException) {
	if (isEmtpy()) {
		throw StackException("StackException:stack underflow");
	} else {
		n--;
		return items[n];
	}
}

int Stack::peek(const throw())

// Not finished
class StackException: public std::logic.error {
public:
	StackException(const std::string& msg = "") : logic_error(msg.c_str())
	{}
};

/**
 * template <typename T>
 * void Stack<T>::push(T x){
 *
 * }
 * template <typename T>
 * T Stack<T>::pop throw(StackException) {
 * 		if (isEmpty()) throw StackException;
 * }
 */
