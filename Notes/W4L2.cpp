/*
 * A fct expressing the relationship between n and t can be complex so we approximate with a measure of complexity call asymptotic complexity
 * f(n) = (n^2) + 100n + logn + 1000
 * for small n, 1000n dominates
 * for large n, (n^2) dominates
 */
