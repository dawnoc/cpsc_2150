/*
 * Assignment #3, CPSC 2150
 * Student Last Name:   Van
 * Student First Name:  Don
 * Student Number:      100300669
 * @version 04/10/18
 * Program to test to see if functions of LList are functioning properly
 */
#include <iostream>
#include <string>
#include "LList.h"
using std::cin;
using std::cout;
using std::endl;
using std::string;

/**
* Compares 2 string to see if their equal and reports the result to standard output
* @param out		The actual output of the function
* @param expected	The expected output of the function
*/
void test(string out, string expected) {
    cout << "output:  \t" << out << endl;
    cout << "expected:\t" << expected << endl;
    if (out.compare(expected) == 0) {
        cout << "As expected" << endl
             << endl;
    } else {
        cout
            << "Incorrect Output________________________________________________________________________"
            << endl
            << endl;
    }
}

/**
* Compares 2 integers/booleans to see if their equal and reports the result to standard output
* @param out		The actual output of the function
* @param expected	The expected output of the function
*/
void test(int out, int ex) {
    cout << "output:  \t" << out << endl;
    cout << "expected:\t" << ex << endl;
    if (out == ex) {
        cout << "As expected" << endl
             << endl;
    } else {
        cout
            << "Incorrect Output________________________________________________________________________"
            << endl
            << endl;
    }
}

int main() {
    LList list;

    test(list.isEmpty(), true);
    list.reverse();
    test(list.toString(), "[ ]");
    test(list.length(), 0);
    test(list.cons('a'), true);
    test(list.toString(), "[ a ]");
    test(list.length(), 1);
    test(list.remove('a'), true);
    test(list.toString(), "[ ]");
    test(list.length(), 0);
    test(list.append('a'), true);
    test(list.toString(), "[ a ]");
    test(list.length(), 1);
    test(list.remove('a'), true);
    test(list.toString(), "[ ]");
    test(list.length(), 0);
    test(list.remove('a'), false);
    test(list.toString(), "[ ]");
    test(list.length(), 0);
    test(list.isEmpty(), true);
    test(list.append('a'), true);
    test(list.toString(), "[ a ]");
    test(list.length(), 1);
    test(list.isEmpty(), false);
    test(list.append('b'), true);
    test(list.toString(), "[ a b ]");
    test(list.length(), 2);
    list.reverse();
    test(list.toString(), "[ b a ]");
    test(list.length(), 2);
    list.reverse();
    test(list.toString(), "[ a b ]");
    test(list.length(), 2);
    test(list.append('c'), true);
    test(list.toString(), "[ a b c ]");
    test(list.length(), 3);
    test(list.cons('c'), true);
    test(list.toString(), "[ c a b c ]");
    test(list.length(), 4);
    test(list.remove('c'), true);
    test(list.toString(), "[ a b c ]");
    test(list.length(), 3);
    test(list.search('a'), true);
    test(list.search('j'), false);
    list.reverse();
    test(list.toString(), "[ c b a ]");
    test(list.length(), 3);
    list.reverse();
    test(list.toString(), "[ a b c ]");
    test(list.length(), 3);
    // Testing Copy constructor
    LList q = list;
    test(q.toString(), "[ a b c ]");
    list.remove('a');
    test(list.toString(), "[ b c ]");
    test(q.toString(), "[ a b c ]");
    // Testing overloaded operator
    list = q;
    test(q.toString(), "[ a b c ]");
    list.remove('a');
    test(list.toString(), "[ b c ]");
    test(q.toString(), "[ a b c ]");
	q.append('d');
	q.cons('3');
	q.append('e');
	q.cons('2');
	q.append('f');
	q.cons('1');
	test(q.toString(), "[ 1 2 3 a b c d e f ]");
	q.append('d');
	q.cons('3');
	q.append('e');
	q.cons('2');
	q.append('f');
	q.cons('1');
	test(q.toString(), "[ 1 2 3 1 2 3 a b c d e f d e f ]");
	q.remove('d');
	q.remove('3');
	q.remove('e');
	q.remove('2');
	q.remove('f');
	q.remove('1');
	test(q.toString(), "[ 1 2 3 a b c d e f ]");
    list.cons('a');
    list.append('d');
    list.append('e');
    list.append('f');
    test(list.toString(), "[ a b c d e f ]");
#ifndef NDEBUG
    list.dumpNodesArray();
#endif
    return 0;
}
