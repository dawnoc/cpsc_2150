/*
 * Assignment #3, CPSC 2150
 * Student Last Name:   Van
 * Student First Name:  Don
 * Student Number:      100300669
 * @version 04/10/18
 * LList.cpp is a singly linked list with memory simulated by an array
 */
#include "LList.h"

#include <iostream>

LList::LList() {
    head = NPTR;
    free = 0;
    mem = new Node[defaultLength];
    for (int i = 0; i < defaultLength; i++) {
        if (i == defaultLength - 1) {
            mem[i] = Node{UNINITIALIZED_ITEM, NPTR};
        } else {
            mem[i] = Node{UNINITIALIZED_ITEM, i + 1};
        }
    }
}

bool LList::isEmpty() const {
    return head == NPTR;
}

int LList::length() const {
    if (isEmpty()) {
        return 0;
    }
    int temp = head;
    int length = 1;
    while (mem[temp].next != NPTR) {
        length++;
        temp = mem[temp].next;
    }
    return length;
}

int LList::memLength() const {
    if (isEmpty()) {
        return 0;
    }
    int temp = head;
    int length = 1;
    while (temp != NPTR) {
        length++;
        temp = mem[temp].next;
    }
    temp = free;
    if (free != NPTR) {
        while (mem[temp].next != NPTR) {
            length++;
            temp = mem[temp].next;
        }
    }
    return length;
}

void LList::expand() {
    int n = memLength();
    Node *temp = new Node[n * 2 - 2];
    for (int i = 0; i < n * 2 - 2; i++) {
        if (i < n - 1) {
            temp[i] = mem[i];
        } else {
            temp[i] = Node{UNINITIALIZED_ITEM, free};
            free = i;
        }
    }
    delete mem;
    mem = temp;
}

bool LList::cons(char ch) {
    try {
        if (free == NPTR) {
            expand();
        }
        if (head == NPTR) {
            head = free;
            free = mem[free].next;
            mem[head].next = NPTR;
        } else {
            int temp = head;
            head = free;
            free = mem[free].next;
            mem[head].next = temp;
        }
        mem[head].item = ch;
        nodesAllocated++;
        return true;
    } catch (const std::bad_alloc &) {
        std::cerr << "in cons = operator,not enough heap mem " << nodesAllocated << " bye, exiting the program";
        exit(4);
    }
}
bool LList::append(char ch) {
    try {
        if (head == NPTR) {
            return cons(ch);
        }
        int last = head;
        while (mem[last].next != NPTR) {
            last = mem[last].next;
        }
        if (free == NPTR) {
            expand();
        }
        mem[last].next = free;
        last = free;
        free = mem[free].next;
        mem[last].next = NPTR;
        mem[last].item = ch;
        nodesAllocated++;
        return true;
    } catch (const std::bad_alloc &) {
        std::cerr << "in append = operator,not enough heap mem " << nodesAllocated << " bye, exiting the program";
        exit(5);
    }
}
bool LList::remove(char ch) {
    if (isEmpty()) {
        return false;
    }
    int temp = head;
    if (mem[head].item == ch) {
        head = mem[head].next;
        mem[temp].next = free;
        free = temp;
        mem[free].item = UNINITIALIZED_ITEM;
        return true;
    }
    while (mem[temp].next != NPTR) {
        if (mem[mem[temp].next].item == ch) {
            int after = mem[mem[temp].next].next;
            mem[mem[temp].next].next = free;
            free = mem[temp].next;
            mem[temp].next = after;
            mem[free].item = UNINITIALIZED_ITEM;
            return true;
        }
        temp = mem[temp].next;
    }
    return false;
}
bool LList::search(char ch) const {
    int temp = head;
    if (mem[head].item == ch) {
        return true;
    }
    while (mem[temp].next != NPTR) {
        if (mem[mem[temp].next].item == ch) {
            return true;
        }
        temp = mem[temp].next;
    }
    return false;
}
void LList::reverse() {
    if (head == NPTR || mem[head].next == NPTR) {
        return;
    }
    int prev = NPTR;
    int curr = head;
    int temp;
    while (mem[curr].next != NPTR) {
        temp = mem[curr].next;
        mem[curr].next = prev;
        prev = curr;
        curr = temp;
    }
    mem[curr].next = prev;
    head = curr;
}

std::string LList::toString(Node *mem, int n) {
    if (n == NPTR) {
        return "";
    }
    char ch = mem[n].item;
    std::string str = ch + std::string("");
    return str + " " + toString(mem, mem[n].next);
}

std::string LList::toString() const {
    // std::string str = "[ ";
    // for (int i = 0; i < memLength(); i++) {
    //     char ch = mem[i].item;
    //     str += ch + std::string("") + " ";
    // }
    // str += "]";
    // std::cout << memLength() << str << std::endl;
    return "[ " + toString(mem, head) + "]";
}

void LList::copyList(Node *copy, int n, int hCopy, int fCopy) {
    head = hCopy;
    free = fCopy;
    for (int i = 0; i < n; i++) {
        mem[i] = Node{copy[i].item, copy[i].next};
    }
}

// copy constructor
LList::LList(const LList &other) {
    try {
        mem = new Node[other.memLength()];
        copyList(other.mem, other.memLength(), other.head, other.free);
    } catch (const std::bad_alloc &) {
        std::cerr << "in copy constructor: not enough heap mem -- bye, exiting the program";
        exit(2);
    }
}

// destructor
LList::~LList() {
    // for (int i = 0; i < memLength(); i++) {
    //     delete mem[i];
    // }
    delete[] mem;
    delete this;
}

// overload the assignment operator
LList &LList::operator=(const LList &other) {
    try {
        if (this != &other) {
            delete[] mem;
            mem = new Node[other.memLength()];
            copyList(other.mem, other.memLength(), other.head, other.free);
        }
        return *this;
    } catch (const std::bad_alloc &) {
        std::cerr << "in overloaded = operator,not enough heap mem -- bye, exiting the program";
        exit(3);
    }
    return *this;
}

#ifndef NDEBUG
using std::cerr;
void LList::dumpNodesArray() const {
    cerr << "head = " << head << "\tfree = " << free << "\n";
}
#endif
