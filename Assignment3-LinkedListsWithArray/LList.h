#ifndef LLIST_H
#define LLIST_H
/*
 * Assignment #3, CPSC 2150
 * Student Last Name:   Van
 * Student First Name:  Don
 * Student Number:      100300669
 * @version 04/10/18
 * LList.h holds a singly linked list with memory simulated by an array
 */

#include <string>
class LList {
   public:
    // build a list
    LList();
    // determine if the list is empty
    bool isEmpty() const;
    // give the length of the list
    int length() const;
    // insert ch at the beginning (at the front) of the linked list
    // return true if successful, false otherwise
    bool cons(char ch);
    // append ch to the end of the linked list
    // return true if successful, false otherwise
    bool append(char ch);
    // delete the first occurrence of ch in the list
    // if the deletion happens, return true, false otherwise
    bool remove(char ch);
    // search ch in the list, return true if found, false otherwise
    bool search(char ch) const;
    // mutator method (function) that reverses the list
    void reverse();
    // returns a string consisting of all the characters of the list
    // in the order in which they are in the list
    // the string returned starts with a "[" followed by a blank
    // followed by each character of the list with its subsequent blank
    // and finishes with a "]"
    // e.g. the string returned would be  "[ a b d ]"
    std::string toString() const;
    // copy constructor...
    // if there is not enough heap memory,
    // an error message will be printed to std::cerr
    // and the program will abort!!!!
    LList(const LList&);
    // overload the assignment operator...
    // if there is not enough heap memory,
    // an error message will be printed to std::cerr
    // and the program will abort!!!!
    LList& operator=(const LList& other);
    // destructor
    ~LList();
#ifndef NDEBUG
    // dump the array, print the contents with std::cerr
    void dumpNodesArray() const;
    long long int nodesAllocated = 0;
    // initialize the item field to UNINITIALIZED_ITEM so that when we are
    // printing the array, we see a # to indicate that the field is not
    // initialized
    static const char UNINITIALIZED_ITEM = '#';
#endif
   private:
    // index of first node in the linked list
    int head;
    // index of the first node of the 'free linked list'
    int free;

    // int version of "Nullptr" for node.next
    static const int NPTR = -1;
    // The initial length
    static const int defaultLength = 5;

    struct Node {
        char item;
        int next;
    };

    // The array of Nodes
    Node* mem;

    // give the length of the list
    int memLength() const;

    // Give the array more memory if it has run out of space
    void expand();

    // returns a string consisting of all the characters of the list
    // in the order in which they are in the list
    // the string returned starts with a "[" followed by a blank
    // followed by each character of the list with its subsequent blank
    // and finishes with a "]"
    // e.g. the string returned would be  "[ a b d ]"
    static std::string toString(Node* mem, int n);

    // Copies the value of one Node array to the other
    void copyList(Node* mem, int length, int head, int free);
};
#endif
