/*
 *  Assignment #2, CPSC 2150
 * Student Last Name:	Van
 * Student First Name:	Don
 * Student Number:		100300669
 * @version 25/09/18
 * Tests to see if all public functions of LList are functioning as expected
 */
#include "LList.h"
#include <iostream>
#include <string>
using std::cout;
using std::endl;
using std::cin;
using std::string;

/**
* Compares 2 string to see if their equal and reports the result to standard output
* @param out		The actual output of the function
* @param expected	The expected output of the function
*/
void test(string out, string expected) {
	cout << "output:  \t" << out << endl;
	cout << "expected:\t" << expected << endl;
	if (out.compare(expected) == 0) {
		cout << "As expected" << endl << endl;
	} else {
		cout
				<< "Incorrect Output________________________________________________________________________"
				<< endl << endl;
	}
}

/**
* Compares 2 integers/booleans to see if their equal and reports the result to standard output
* @param out		The actual output of the function
* @param expected	The expected output of the function
*/
void test(int out, int ex) {
	cout << "output:  \t" << out << endl;
	cout << "expected:\t" << ex << endl;
	if (out == ex) {
		cout << "As expected" << endl << endl;
	} else {
		cout
				<< "Incorrect Output________________________________________________________________________"
				<< endl << endl;
	}
}

int main() {
	LList p;
	
	test(p.isEmpty(), true);
	p.reverse();
	test(p.toString(), "[ ]");
	test(p.length(), 0);
	test(p.cons('a'), true);
	test(p.toString(), "[ a ]");
	test(p.length(), 1);
	test(p.remove('a'), true);
	test(p.toString(), "[ ]");
	test(p.length(), 0);
	test(p.append('a'), true);
	test(p.toString(), "[ a ]");
	test(p.length(), 1);
	test(p.remove('a'), true);
	test(p.toString(), "[ ]");
	test(p.length(), 0);
	test(p.remove('a'), false);
	test(p.toString(), "[ ]");
	test(p.length(), 0);
	test(p.isEmpty(), true);
	test(p.append('a'), true);
	test(p.toString(), "[ a ]");
	test(p.length(), 1);
	test(p.isEmpty(), false);
	test(p.append('b'), true);
	test(p.toString(), "[ a b ]");
	test(p.length(), 2);
	p.reverse();
	test(p.toString(), "[ b a ]");
	test(p.length(), 2);
	p.reverse();
	test(p.toString(), "[ a b ]");
	test(p.length(), 2);
	test(p.append('c'), true);
	test(p.toString(), "[ a b c ]");
	test(p.length(), 3);
	test(p.cons('c'), true);
	test(p.toString(), "[ c a b c ]");
	test(p.length(), 4);
	test(p.remove('c'), true);
	test(p.toString(), "[ a b c ]");
	test(p.length(), 3);
	test(p.search('a'), true);
	test(p.search('j'), false);
	p.reverse();
	test(p.toString(), "[ c b a ]");
	test(p.length(), 3);
	p.reverse();
	test(p.toString(), "[ a b c ]");
	test(p.length(), 3);
	// Testing Copy constructor
	LList q = p;
	test(q.toString(), "[ a b c ]");
	p.remove('a');
	test(p.toString(), "[ b c ]");
	test(q.toString(), "[ a b c ]");
	// Testing overloaded operator
	p = q;
	test(q.toString(), "[ a b c ]");
	p.remove('a');
	test(p.toString(), "[ b c ]");
	test(q.toString(), "[ a b c ]");
	return 0;
}
