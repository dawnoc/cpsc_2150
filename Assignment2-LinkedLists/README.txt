 Assignment #2, CPSC 2150
Student Last Name:	Van
Student First Name:	Don
Student Number:		100300669

1)	I used cons to build the longest linked list because cons has a constant runtime while append has a runtime of n
2)	Approximately 127 350 549 (Did not run multiple tests)
		If I were to remove Line 31 of LList.cpp 
		"cerr << nodesAllocated << endl;" 
		my computer would freeze due to attempting to create 120 billion in an unneccesarily small time.
		Otherwise it ran till a bad_alloc& was caught