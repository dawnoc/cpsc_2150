/*
 * Assignment #2, CPSC 2150
 * Student Last Name:	Van
 * Student First Name:	Don
 * Student Number:		100300669
 * @version 25/09/18
 * Program to test what the longest list is, namely, the most number of nodes.
 */
#include "LList.h"
#include <iostream>
#include <climits>

using std::cout;

int main() {
   LList list;
   long long int count = 0;
   // counts until nodesAllocated runs out of space
   while (count < LLONG_MAX) {
	   list.cons('a');
   }
   return 0;
}
