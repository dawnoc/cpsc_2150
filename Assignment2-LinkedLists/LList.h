#ifndef LLIST_H
#define LLIST_H
/* 
 * Assignment #2, CPSC 2150
 * Student Last Name:	Van
 * Student First Name:	Don
 * Student Number:		100300669
 * @version 25/09/18
 * LList.h
 */

// when we do NOT want to debug, we do #define
// #define NDEBUG
// we comment out #define NDEBUG when we do not want debug statements
#include <string>
class LList {
public:
	// build a list
	LList();
	// determine if the list is empty
	bool isEmpty() const;
	// give the length of the list
	int length() const;
	// insert ch at the beginning (at the front) of the linked list
	// return true if successful, false otherwise
	bool cons(char ch);
	// append ch to the end of the linked list
	// return true if successful, false otherwise
	bool append(char ch);
	// delete the first occurrence of ch in the list
	// if the deletion happens, return true, false otherwise
	bool remove(char ch);
	// search ch in the list, return true if found, false otherwise
	bool search(char ch) const;
	// mutator method (function) that reverses the list
	void reverse();
	// returns a string consisting of all the characters of the list
	// in the order in which they are in the list
	// the string returned starts with a "[" followed by a blank
	// followed by each character of the list with its subsequent blank
	// and finishes with a "]"
	// e.g. the string returned would be  "[ a b d ]"
	std::string toString() const;
	// copy constructor...
	// if there is not enough heap memory,
	// an error message will be printed to std::cerr
	// and the program will abort!!!!
	LList(const LList&);
	// overload the assignment operator...
	// if there is not enough heap memory,
	// an error message will be printed to std::cerr
	// and the program will abort!!!!
	LList& operator =(const LList& other);
	// destructor
	~LList();
private:
	struct Node {
		char item;
		Node* next;
	};
	// pointer to the linked list
	Node* head;

	// for the assignment, you are NOT allowed to have a tail pointer
	// i.e. one that points to the last node of the list

	// get the length of the list p
	static int length(Node *p);
	static int length(Node *p, int n);

	// append ch to the end of the linked list
	// return the appended ch
	static bool append(char ch, Node *p);

	// delete the first occurrence of ch in the list
	// if the deletion happens, return true, false otherwise
	static bool remove(char ch, Node *p);

	// search ch in the list, return true if found, false otherwise
	static bool search(char ch, Node *p);

	// create a new Node with ch as the item field and with a link to p,
	// and return a pointer to the newly created Node
	static Node* cons(char ch, Node *p);

	// mutator method (function) that reverses the list
	void reverse(Node *prev, Node *curr);

	// returns a string consisting of all the characters of the list
	// in the order in which they are in the list
	// the string returned starts with a "[" followed by a blank
	// followed by each character of the list with its subsequent blank
	// and finishes with a "]"
	// e.g. the string returned would be  "[ a b d ]"
	static std::string toString(Node *p);

	// returns a new Linked List with the same items as p
	static Node* copyList(const Node *p);
	
	// returns the head of the deleted linked list
	// expected to always be nullptr
	static Node* deleteList(Node *p);

#ifndef NDEBUG
	long long int nodesAllocated = 0;
#endif
};
#endif
