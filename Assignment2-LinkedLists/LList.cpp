/* 
 * Assignment #2, CPSC 2150
 * Student Last Name:	Van
 * Student First Name:	Don
 * Student Number:		100300669
 * @version 25/09/18
 * LList.cpp is a Singly Linked List class
 */

#include <string>
#include "LList.h"

using std::string;

#ifndef NDEBUG
#include <iostream>
using std::cerr;
using std::endl;
#endif

LList::LList() {
	head = nullptr;
}

bool LList::cons(char ch) {
	try {
		// since the exception happens in cons,
		//head is only adjusted if cons succeeds
		#ifndef NDEBUG
		nodesAllocated++;
		cerr << nodesAllocated << endl;
		#endif
		head = cons(ch, head);
		return true;
	} catch (const std::bad_alloc&) {
		#ifndef NDEBUG
		cerr << "***cons exhausts the heap with " << nodesAllocated;
		cerr << " nodes\n\n";
		#endif
	}
	return false;
}

// precondition: p has been properly initialized
LList::Node* LList::cons(char ch, Node *p) {
	return new Node { ch, p };
}

bool LList::isEmpty() const {
	if (head == nullptr) {
		return true;
	}
	return false;
}

int LList::length() const {
	return length(head, 0);
}

int LList::length(Node *p, int n) {
	if (p == nullptr) {
		return n;
	}
	return length(p->next, n + 1);
}

//int LList::length(Node *p) {
//	if (p == nullptr) {
//		return 0;
//	}
//	if (p->next == nullptr) {
//		return 1;
//	}
//	return length(p->next, 1);
//}

bool LList::append(char ch, Node *p) {
	if (p->next == nullptr) {
		p->next = cons(ch, nullptr);
		return true;
	}
	return append(ch, p->next);
}

bool LList::append(char ch) {
	if (isEmpty()) {
		head = cons(ch, nullptr);
		return true;
	}
	return append(ch, head);
}

bool LList::remove(char ch, Node *p) {
	if (p->next == nullptr) {
		return false;
	}
	if (p->next->item == ch) {
		Node *temp = p->next;
		p->next = temp->next;
		delete temp;
		return true;
	}
	return remove(ch, p->next);
}

bool LList::remove(char ch) {
	if (head == nullptr) {
		return false;
	}
	if (head->item == ch) {
		Node *temp = head;
		head = head->next;
		delete temp;
		return true;
	}
	return remove(ch, head);
}

bool LList::search(char ch, Node *p) {
	if (p == nullptr) {
		return false;
	}
	if (p->item == ch) {
		return true;
	}
	return search(ch, p->next);
}

bool LList::search(char ch) const {
	return search(ch, head);
}

void LList::reverse(Node* prev, Node* curr) {
	if (curr->next == nullptr) {
		curr->next = prev;
		head = curr;
	} else {
		Node* temp = curr->next;
		curr->next = prev;
		reverse(curr, temp);
	}
}

void LList::reverse() {
	if (head == nullptr || head->next == nullptr) {
		return;
	}
	Node* temp = head->next;
	head->next = nullptr;
	reverse(head, temp);
}

std::string LList::toString(Node *p) {
	if (p == nullptr) {
		return "";
	}
	char ch = p->item;
	std::string str = ch + std::string("");
	return str + " " + toString(p->next);
}

std::string LList::toString() const {
	return "[ " + toString(head) + "]";
}

LList::Node* LList::copyList(const Node *p) {
	if (p == nullptr) {
		return nullptr;
	}
	return cons(p->item, copyList(p->next));
}

LList::Node* LList::deleteList(Node *p) {
	if (p == nullptr || p->next == nullptr) {
		delete p;
		return nullptr;
	}
	Node* temp = p->next;
	delete p;
	return deleteList(temp);
}

//LList::Node* LList::deleteList(Node *p) {
//	Node* temp;
//	while (p != nullptr) {
//		temp = p->next;
//		delete p;
//		p = temp;
//	}
//	return nullptr;
//}

LList::LList(const LList& other) {
	head = copyList(other.head);
}

LList& LList::operator =(const LList& rtSide) {
	if (this != &rtSide) {
		head = deleteList(head);
		head = copyList(rtSide.head);
	}
	return *this;
}

LList::~LList() {
	head = deleteList(head);
}