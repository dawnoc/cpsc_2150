// Assignment 2 - Battleship
// Name: Matthew Hansen
// Date: September 25, 2018
//____________________________________________

#include <ctime>
#include <cstdlib>
#include <iostream>
#include <iomanip>
using namespace std;

//Sets the number of columns for the array
const int COLUMN_SIZE = 10;

//Global variable to be used later to check if we should randomize play
int enableRandom;

//Generates better random numbers
int betterRand()
{
	int x = rand();
	int y = rand();
	return (x << 15) | y;
}

// Checks the next few spaces ahead in the row, and depending if it is free or not it returns true or false
bool rowscoutPlacement(char arr[][COLUMN_SIZE], int length, int rowstart,
		int columnstart)
{
	for (int i = 0; i < length; i++)
	{
		if (rowstart + i > 9 || arr[rowstart + i][columnstart] != '-')
		{
			return false;
		}
	}
	return true;
}

// Checks the next few spaces down in the column, and depending if it is free or not it returns true or false
bool columnscoutPlacement(char arr[][COLUMN_SIZE], int length, int rowstart,
		int columnstart)
{
	for (int i = 0; i < length; i++)
	{
		if (columnstart + i > 9 || arr[rowstart][columnstart + i] != '-')
		{
			return false;
		}
	}
	return true;
}

//Places ships in the grid
void placeShips(char arr[][COLUMN_SIZE], char ship, int length)
{
	int rowstart = betterRand() % 10;
	int columnstart = betterRand() % 10;

	//Flip a coin to decide horizonal or vertical placement
	int coinflip = betterRand() % 2;

	if (coinflip == 0)
	{
		//Horizontal Placement
		while (rowscoutPlacement(arr, length, rowstart, columnstart) != true)
		{
			rowstart = betterRand() % 10;
			columnstart = betterRand() % 10;
		}

		for (int i = 0; i < length; i++)
		{
			arr[rowstart + i][columnstart] = ship;
		}
	}

	if (coinflip == 1)
	{
		//Vertical Placement
		while (columnscoutPlacement(arr, length, rowstart, columnstart) != true)
		{
			rowstart = betterRand() % 10;
			columnstart = betterRand() % 10;
		}

		for (int i = 0; i < length; i++)
		{
			arr[rowstart][columnstart + i] = ship;
		}
	}
}

//Prints the board out, showing your ships while keeping the enemy ships hidden
void displayGrid(char arr[][COLUMN_SIZE], char arr2[][COLUMN_SIZE])
{
	//Prints letter labelling system for columns
	cout << "  ABCDEFGHIJ  " << "  ABCDEFGHIJ" << endl;

	for (int i = 0; i < COLUMN_SIZE; i++)
	{
		//Prints number labelling system for rows
		cout << i << "|";

		//Prints your grid and ships
		for (int j = 0; j < 10; j++)
		{
			cout << arr[i][j];
		}

		cout << "\t";

		//Prints the enemy grid, but keeps their ships hidden
		for (int k = 0; k < 10; k++)
		{
			if (arr2[i][k] == 'C' || arr2[i][k] == 'B' || arr2[i][k] == 'K'
					|| arr2[i][k] == 'S' || arr2[i][k] == 'D')
				cout << '-';

			else
				cout << arr2[i][k];
		}
		cout << endl;
	}
}

//Starts the player turn
void playerTurn(char arr[][COLUMN_SIZE])
{
	char columnletter;
	int row;

	//(Enter a capital letter for this part, recommended caps lock)
	cout << "Choose the column(Capital letter from A to J): " << endl;
	cin >> columnletter;

	//Get a integer based on inputted letter
	int column = columnletter - 65;

	cout << "Choose the row(number from 0 to 9): " << endl;
	cin >> row;

	//Enable computer vs computer if the row number is -1
	if (row == -1)
	{
		enableRandom++;
	}

	//Display your guess
	cout << "You guessed " << columnletter << row << "." << endl;

	//If the guess is out of bounds, randomize a guess instead
	if (row > 9 || row < 0 || column < 0 || column > 9)
	{
		cout << "Invalid guess, randomizing new guess: " << endl;
		column = betterRand() % 10;
		row = betterRand() % 10;

		char columnletter = column + 'A';

		cout << "Your randomized guess is " << columnletter << row << "."
				<< endl;
	}

	//Check for hit or miss (or if you hit a place where you've already hit), mark the grid, and display the outcome
	if (arr[row][column] == 'C' || arr[row][column] == 'B'
			|| arr[row][column] == 'K' || arr[row][column] == 'S'
			|| arr[row][column] == 'D')
	{
		cout << "You Hit!" << endl;
		arr[row][column] = 'H';
	}

	else if (arr[row][column] == 'H')
	{
		cout << "You Hit a Sunken Ship!" << endl;
	}

	else
	{
		cout << "You Missed." << endl;
		arr[row][column] = 'X';
	}
}

//Starts the computer turn
void computerTurn(char arr[][COLUMN_SIZE])
{
	int column = betterRand() % 10;
	int row = betterRand() % 10;

	char columnletter = column + 'A';

	//Display the randomly generated guess
	cout << "They guessed " << columnletter << row << "." << endl;

	//Check for hit or miss (or if you hit a place where you've already hit), mark the grid, and display the outcome
	if (arr[row][column] == 'C' || arr[row][column] == 'B'
			|| arr[row][column] == 'K' || arr[row][column] == 'S'
			|| arr[row][column] == 'D')
	{
		cout << "They Hit!" << endl;
		arr[row][column] = 'H';
	}

	else if (arr[row][column] == 'H')
	{
		cout << "They Hit a Sunken Ship!" << endl;
	}

	else
	{
		cout << "They Missed." << endl;
		arr[row][column] = 'X';
	}
}

//Checks the win condition, if there is no more ships, return true, otherwise return false
bool checkforVictory(char arr[][COLUMN_SIZE])
{
	for (int i = 0; i < 10; i++)
	{
		for (int j = 0; j < 10; j++)
		{
			if (arr[i][j] == 'C' || arr[i][j] == 'B' || arr[i][j] == 'K'
					|| arr[i][j] == 'S' || arr[i][j] == 'D')
				return false;
		}
	}

	return true;
}

int main()
{
	//Initialize player and enemy grids with '-' for all parts
	srand(time(0));
	const int ROW_SIZE = 10;
	char playerGrid[ROW_SIZE][COLUMN_SIZE];
	char enemyGrid[ROW_SIZE][COLUMN_SIZE];

	for (int i = 0; i < 10; i++)
	{
		for (int j = 0; j < 10; j++)
		{
			playerGrid[i][j] = '-';
		}
	}

	for (int i = 0; i < 10; i++)
	{
		for (int j = 0; j < 10; j++)
		{
			enemyGrid[i][j] = '-';
		}
	}

	//Place ships in the player and enemy grids
	placeShips(playerGrid, 'C', 5);
	placeShips(playerGrid, 'B', 4);
	placeShips(playerGrid, 'K', 3);
	placeShips(playerGrid, 'S', 3);
	placeShips(playerGrid, 'D', 2);

	placeShips(enemyGrid, 'C', 5);
	placeShips(enemyGrid, 'B', 4);
	placeShips(enemyGrid, 'K', 3);
	placeShips(enemyGrid, 'S', 3);
	placeShips(enemyGrid, 'D', 2);

	//Display the starting grids
	cout << "Starting Board: " << endl;

	displayGrid(playerGrid, enemyGrid);

	//As long as the victory condition for either grid is false, continue the game
	while (checkforVictory(enemyGrid) == false
			&& checkforVictory(playerGrid) == false)
	{
		//Play a random game if -1 was ever entered
		if (enableRandom == 1)
		{
			computerTurn(enemyGrid);
			computerTurn(playerGrid);
			displayGrid(playerGrid, enemyGrid);
		}

		//Take turns playing Battleship, and display the grid at the end
		else
		{
			playerTurn(enemyGrid);
			computerTurn(playerGrid);
			displayGrid(playerGrid, enemyGrid);
		}
	}

	//Once the loop is ended, check which side won
	if (checkforVictory(enemyGrid) == true)
	{
		cout << "Congratulations! You've decimated their ships and won!"
				<< endl;
	}

	else if (checkforVictory(playerGrid) == true)
	{
		cout
				<< "Your opponent has decimated your fleet! You've lost! Better luck next time."
				<< endl;
	}

	return 0;
}
