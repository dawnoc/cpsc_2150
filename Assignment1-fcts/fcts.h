#ifndef FCTS_H
#define FCTS_H

#include <string>
using std::string;

// remove for the assignment the following line with factorial in it
// factorial is here for the lab
int factorial(int n);

// Exercise 3.6
int addDigits(int n);

// Exercise 3.8
void outMinTransformation(int x, int y);

// Exercise 5.5
int maximum(const int A[], int n);

// Exercise 5.8
bool isStrictlyDescending(const int A[], int n);

// Exercise 7.2
int toInteger(const string& str);

// Exercise 7.5
bool equalsNoVowels(const string& strA, const string& strB);

#endif
