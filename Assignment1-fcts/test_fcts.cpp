/*
 * @author	Don Van, 100300669
 * @version	11/09/2018
 * CPSC 2150-001
 * The test file for fcts.cpp
 */
#include <iostream>
#include "fcts.h"
using std::cerr;
using std::cout;
using std::endl;

void test(int out, int ex) {
	cout << "output:  \t" << out << endl;
	cout << "expected:\t" << ex << endl;
	if (out == ex) {
		cout << "As expected" << endl << endl;
	} else {
		cout << "Incorrect Output________________________________________________________________________" << endl << endl;
	}
}

void test_factorial() {
	test(addDigits(2019), 12);
	cout << "output:  \t";
	outMinTransformation(6, 19);
	cout << endl;
	cout << "expected:\t" << "19 = ((((6 + 1) + 1) + 1) * 2 + 1)" << endl
			<< endl;
	cout << "output:  \t";
	outMinTransformation(7, 58);
	cout << endl;
	cout << "expected:\t" << "58 = (7 * 2 * 2 + 1) * 2" << endl << endl;
	int A[5] = { 8710, 3948, 7240, 873, 4923 };
	test(maximum(A, 5), 8710);
	test(isStrictlyDescending(A, 5), false);
	int B[5] = { 3821, 3948, 7240, 8203, 8710 };
	test(maximum(B, 5), 8710);
	test(isStrictlyDescending(B, 5), false);
	int C[5] = { 8710, 8235, 7240, 5392, 2373 };
	test(maximum(C, 5), 8710);
	test(isStrictlyDescending(C, 5), true);
	test((toInteger("423") + toInteger("77")), 500);
	test((toInteger("aaa")), -1);
	test(equalsNoVowels("kangaroo", "kongeroo"), true);
	test(equalsNoVowels("kangaroo", "kaangaro"), true);
	test(equalsNoVowels("kongeroo", "kaangaro"), true);
	test(equalsNoVowels("kangaroo", "kngr"), true);
	test(equalsNoVowels("kongeroo", "kngr"), true);
	test(equalsNoVowels("kaangaro", "kngr"), true);
	test(equalsNoVowels("kaangaro", "kngrr"), false);
	test(equalsNoVowels("kaangaro", "rkngr"), false);
	test(equalsNoVowels("kaangaro", "knrgr"), false);
}

int main() {
	test_factorial();
	return 0;
}
