/*
 * @author	Don Van, 100300669
 * @version	10/09/2018
 * CPSC 2150-001
 * Functions that are used to solve the questions in assignment one from recursion.pdf
 */

#include "fcts.h"
#include <iostream>
#include <string>
#include <cstdlib>

using std::cout;
using std::string;
using std::cerr;
using std::to_string;

/*
 * Takes a positive integer and sums up all the digits
 * @param n			The integer the digits of
 * @return			The sum off all the digits
 * @precondition	n >= 0
 */
int addDigits(int n) {
	if (n <= 0)
		return 0;
	return (n % 10) + addDigits(n / 10);
}

/*
 * Helper Function that writes the equation with the least (*2)'s and (+1)'s to get from point x to y
 * @param x		The initial position
 * @param y		The final position
 * @param str	The equation
 * @return		The final equation
 */
string outMinTransformation(int x, int y, string str) {
	if (y == x) {
		return str;
	}
	if ((y - (x * 4)) >= 0 || (y - (x * 2)) == 1 || ((y) == (x * 2))) {
		return outMinTransformation(x * 2, y, str + " * 2");
	}
	return outMinTransformation(x + 1, y, "(" + str + " + 1)");
}

/*
 * Recursive Function that prints the equation to standard output with the least (*2)'s and (+1)'s to get from point x to y
 * @param x	The initial position
 * @param y	The final position
 */
void outMinTransformation(int x, int y) {
	cout << y << " = ";
	cout << outMinTransformation(x, y, to_string(x));
}

/*
 * Finds the highest value in an array of integers
 * @param A	The array to search through
 * @param n	The length of the array
 * @return	The maximum value in the array
 */
int maximum(const int A[], int n) {
	if (n == 1) {
		return A[0];
	}
	int max = maximum(A, n - 1);
	if (A[n - 1] < max) {
		return max;
	}
	return A[n - 1];
}

/*
 * Checks if the array is strictly descending
 * @param A	The array to check
 * @param n	The length of the array
 * @return	Whether is is descending or not
 */
bool isStrictlyDescending(const int A[], int n) {
	if (n <= 1) {
		return true;
	}
	if (A[n - 1] >= A[n - 2]) {
		return false;
	}
	return isStrictlyDescending(A, n - 1);
}

/*
 * Helper function that converts a string of numbers into a positive integer
 * @param str		The string to convert
 * @param n			The length of the string left to convert
 * @return			The number in integer format
 * @postcondition	Strings will return -1
 */
int toInteger(const string& str, int n) {
	if (n < 1) {
		return 0;
	}
	if (!isdigit(str[n-1]) || toInteger(str, n - 1) == -1){
		return -1;
	}
	return ((str[n - 1]) - '0') + (10 * toInteger(str, n - 1));
}

/*
 * Recursive function that converts a string of numbers into an integer
 * @param str	The string to be converted
 * @return		The integer converted from the string
 * @postcondition	Strings will return -1
 */
int toInteger(const string& str) {
	return toInteger(str, str.length());
}

/*
 * A function called to determine whether ch is a vowel or not
 * @param ch	The character to check
 * @return		Whether the character is a vowel or not
 */
bool isVowel(char ch) {
	switch (ch) {
	case 'a':
	case 'e':
	case 'i':
	case 'o':
	case 'u':
	case 'A':
	case 'E':
	case 'I':
	case 'O':
	case 'U':
		return true;
	default:
		return false;
	}
}

/*
 * Helper function that checks if 2 string are equal disregarding vowels
 * @param strA	The first string to compare
 * @param a		The length of strA
 * @param strB	The second string to compare
 * @param b		The length of strB
 * @return		If they are equal or not
 */
bool equalsNoVowels(const string& strA, int a, const string& strB, int b) {
	if (a <= 0 && b <= 0) {
		return true;
	}
	if (a <= 0 && !isVowel(strB[b - 1])) {
		return false;
	}
	if (b <= 0 && !isVowel(strA[a - 1])) {
		return false;
	}
	if (isVowel(strA[a - 1])) {
		return equalsNoVowels(strA, a - 1, strB, b);
	}
	if (isVowel(strB[b - 1])) {
		return equalsNoVowels(strA, a, strB, b - 1);
	}
	if (strA[a - 1] == strB[b - 1]) {
		return equalsNoVowels(strA, a - 1, strB, b - 1);
	}
	return false;
}

/*
 * Recursive function that checks if 2 string are equal disregarding vowels
 * @param strA	The first string to compare
 * @param strB	The second string to compare
 * @return		If they are equal or not
 */
bool equalsNoVowels(const string& strA, const string& strB) {
	return equalsNoVowels(strA, strA.length(), strB, strB.length());
}
