/*
 * @author	Don Van, 100300669
 * @version	10/25/2018
 * CPSC 2150-001
 * Class holding a grid of "Black and White" spaces with the utility of filling in the white spaces with black.
 * Similar to paint bucket tool.
 * Uses recursion, STL stack, or STL queue to fill
 */

#ifndef FILLC_H
#define FILLC_H

#include <fstream>
#include <string>
using std::ifstream;
using std::ofstream;
using std::string;

class fillColour {
   public:
    const int BLACK = 1;
    const int WHITE = 0;
    /**
     * Class Constructor:   Creates blank grid with dimensions x and y
     * @param x number of columns
     * @param y number of rows
     **/
    fillColour(int x, int y);
    /**
     * Fills in part of grid within the bounds of the grid and surrounding "Black Lines" using recursion
     * @param i The x coordinate of target "pixel"
     * @param j The y coordinate of target "pixel"
     **/
    void recursiveFill(int i, int j);
    /**
     * Fills in part of grid within the bounds of the grid and surrounding "Black Lines" using STL stack
     * @param i The x coordinate of target "pixel"
     * @param j The y coordinate of target "pixel"
     **/
    void stackFill(int i, int j);
    /**
     * Fills in part of grid within the bounds of the grid and surrounding "Black Lines" using STL queue
     * @param i The x coordinate of target "pixel"
     * @param j The y coordinate of target "pixel"
     **/
    void queueFill(int i, int j);
    /**
     * Gets the number of columns of the grid
     * @return  The number of columns
     **/
    int getColumns() const;
    /**
     * Gets the number of rows of the grid
     * @return  The number of rows
     **/
    int getRows() const;
    /**
     * Reads in a sufficient amount of integers from the inputFileStream to fill the grid with the desired pattern
     * @param in        The inputFileStream containing the "Black and White spaces" (Doesn't care about newlines)
     * @param rtSide    The fillColour with the grid that is needed to be filled
     * @return          The inputFileStream without the values taken by the grid
     **/
    friend ifstream& operator>>(ifstream& in, const fillColour& rtSide);
    /**
     * Outputs the grid string to an output file stream
     * @param out       The outputFileStream to send the data to
     * @param rtSide    The fillColour with the grid of desired data
     * @return          The outputFileStream with the grids content properly formatted
     **/
    friend ofstream& operator<<(ofstream& out, const fillColour& rtSide);
    /**
     * Copy constructor
     **/
    fillColour& operator=(const fillColour& other);
    ~fillColour();

   private:
    int x;  // Number of columns
    int y;  // Number of rows
    struct XY {
        int x;
        int y;
    };
    int** Grid;
};
#endif
