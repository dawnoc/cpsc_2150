/*
 * @author	Don Van, 100300669
 * @version	10/25/2018
 * CPSC 2150-001
 * Creates the fillColour class with the desired settings using options.
 * Options include input and output files, the coordinates of where to fill, and the method of filling
 * Similar to paint bucket tool.
 */

#include <cstdlib>
#include <fstream>
#include <iostream>
#include "fillColour.h"
using std::atoi;
using std::cerr;
using std::cout;
using std::endl;
using std::ifstream;
using std::ofstream;

/**
 * Prints out the usage of the file
 **/
void help() {
    cout << "fill.cpp simulates the paint bucket tool using a grid and point" << endl
         << endl
         << "-help" << endl
         << "prints the usage" << endl
         << endl
         << "-input inputFile" << endl
         << "the inputFile has as the first two integers the rows and columns followed by rows x columns values of 1’s and 0’s" << endl
         << endl
         << "-output outputFile" << endl
         << "the outputFile has rows x columns of 1’s and 0’s" << endl
         << endl
         << "-i row#" << endl
         << "the row of the pixel where the filling should start (origin in the upper left hand corner)" << endl
         << endl
         << "-j column#" << endl
         << "the column of the pixel where the filling should start (origin in the upper left hand corner)" << endl
         << endl
         << "-queue" << endl
         << "to call the version of fillBlack that uses the STL queue" << endl
         << endl
         << "-stack" << endl
         << "to call the version of fillBlack that uses the STL stack efficiently" << endl
         << endl
         << "-recursive" << endl
         << "to call the recursive version of fillBlack: note that this is the default version" << endl;
}

const string invalidFile = "...";
const int invalidXY = -1;
const int modeR = 0;
const int modeS = 1;
const int modeQ = 2;

int main(int argc, char* argv[]) {
    int count = 1;
    string in = invalidFile;
    string out = invalidFile;
    int i = invalidXY;
    int j = invalidXY;
    int mode = modeR;
    if (argc == 1) {
        help();
        return 0;
    }
    while (count < argc) {
        string args = argv[count];
        if (!args.compare("-help")) {
            help();
            return 0;
        } else if (!args.compare("-input")) {
            count++;
            in = argv[count];
        } else if (!args.compare("-output")) {
            count++;
            out = argv[count];
        } else if (!args.compare("-i")) {
            count++;
            i = atoi(argv[count]);
        } else if (!args.compare("-j")) {
            count++;
            j = atoi(argv[count]);
        } else if (!args.compare("-queue")) {
            mode = modeQ;
        } else if (!args.compare("-stack")) {
            mode = modeS;
        } else if (!args.compare("-recursive")) {
            mode = modeR;
        } else {
            cout << "INVALID ARGUMENT" << args;
            return 1;
        }
        count++;
    }

    ifstream inputStream(in);
    ofstream outputStream(out);

    if (!inputStream.good() || !outputStream.good()) {
        cout << "STREAMS FAILED" << endl;
        return 1;
    }
    int x, y;
    inputStream >> y;
    inputStream >> x;
    fillColour grid = fillColour(x, y);
    inputStream >> grid;
    switch (mode) {
        case modeR:
            grid.recursiveFill(j, i);
            break;
        case modeS:
            grid.stackFill(j, i);
            break;
        case modeQ:
            grid.queueFill(j, i);
            break;
        default:
            grid.recursiveFill(j, i);
            break;
    }
    outputStream << grid;
    return 0;
}
