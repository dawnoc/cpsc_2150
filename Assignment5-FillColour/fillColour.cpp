/*
 * @author	Don Van, 100300669
 * @version	10/25/2018
 * CPSC 2150-001
 * Class holding a grid of "Black and White" spaces with the utility of filling in the white spaces with black.
 * Similar to paint bucket tool.
 * Uses recursion, STL stack, or STL queue to fill
 */

#include "fillColour.h"
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <queue>
#include <stack>
using std::atoi;
using std::cout;
using std::endl;
using std::ifstream;
using std::ofstream;
using std::stack;
using std::queue;

fillColour::fillColour(int i, int j) {
    x = i;
    y = j;
    // cout << x << "," << y << endl;
    Grid = new int*[x];
    // int** Grid = new int*[y];
    for (int k = 0; k < x; ++k) {
        Grid[k] = new int[y];
    }
}

void fillColour::recursiveFill(int i, int j) {
    if (i < 0 || i >= x || j < 0 || j >= y) {
        return;
    }
    if (Grid[i][j] == BLACK) {
        return;
    }
    Grid[i][j] = BLACK;
    recursiveFill(i + 1, j);
    recursiveFill(i, j + 1);
    recursiveFill(i - 1, j);
    recursiveFill(i, j - 1);
}

void fillColour::stackFill(int i, int j) {
    if (i < 0 || i >= x || j < 0 || j >= y) {
        return;
    }

    if (Grid[i][j] == BLACK) {
        return;
    }

    stack<XY> depthFirst;
    depthFirst.push(XY{i, j});
    while (!depthFirst.empty()) {
        XY temp = depthFirst.top();
        depthFirst.pop();
        int k = temp.x;
        int l = temp.y;
        if (k >= 0 && k < x && j >= 0 && j < y) {
            if (Grid[k][l] == WHITE) {
                Grid[k][l] = BLACK;
                depthFirst.push(XY{k + 1, l});
                depthFirst.push(XY{k, l + 1});
                depthFirst.push(XY{k - 1, l});
                depthFirst.push(XY{k, l - 1});
            }
        }
    }
}

void fillColour::queueFill(int i, int j) {
    if (i < 0 || i >= x || j < 0 || j >= y) {
        return;
    }

    if (Grid[i][j] == BLACK) {
        return;
    }

    queue<XY> breadthFirst;
    breadthFirst.push(XY{i, j});
    while (!breadthFirst.empty()) {
        XY temp = breadthFirst.front();
        breadthFirst.pop();
        int k = temp.x;
        int l = temp.y;
        if (k >= 0 && k < x && j >= 0 && j < y) {
            if (Grid[k][l] == WHITE) {
                Grid[k][l] = BLACK;
                breadthFirst.push(XY{k + 1, l});
                breadthFirst.push(XY{k, l + 1});
                breadthFirst.push(XY{k - 1, l});
                breadthFirst.push(XY{k, l - 1});
            }
        }
    }
}

int fillColour::getColumns() const {
    return y;
}

int fillColour::getRows() const {
    return x;
}

ifstream& operator>>(ifstream& in, const fillColour& rtSide) {
    for (int i = 0; i < rtSide.getColumns(); i++) {
        for (int j = 0; j < rtSide.getRows(); j++) {
            in >> rtSide.Grid[j][i];
        }
    }
    return in;
}

ofstream& operator<<(ofstream& out, const fillColour& rtSide) {
    for (int i = 0; i < rtSide.y; i++) {
        for (int j = 0; j < rtSide.x; j++) {
            out << rtSide.Grid[j][i] << " ";
        }
        out << endl;
    }
    return out;
}

fillColour& fillColour::operator=(const fillColour& other) {
    if (this != &other) {
        for (int i = 0; i < x; ++i) {
            delete[] Grid[i];
        }
        delete[] Grid;
        x = other.getColumns();
        y = other.getRows();
        Grid = new int*[x];
        for (int k = 0; k < x; ++k) {
            Grid[k] = new int[y];
        }
        for (int i = 0; i < y; i++) {
            for (int j = 0; j < x; j++) {
                Grid[j][i] = other.Grid[j][i];
            }
        }
    }
    return *this;
}

fillColour::~fillColour() {
    for (int i = 0; i < x; ++i) {
        delete[] Grid[i];
    }
    delete[] Grid;
}